#!/bin/bash

export GODEBUG=cgocheck=0
export LD_LIBRARY_PATH=$(pwd)

while [ "1" == "1" ];
do
    ./sagent_gamania_bgo agent_gamania_bgo.conf
    if [[ $? -ne 101 ]]; then
        exit
    fi
    echo "Restart streamer after 1 minute..."
    sleep 60
done
