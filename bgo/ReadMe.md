# Streamer gamania_bgo 的開發

## Feature


# Change Log
==========================

- 0.0
    - [0.0.0]
        - 最原始版本
    - [0.0.1]
        - 整理過struct和順序, 方便我核對欄位 以及 核對IOS和Android的資料結構
    - [0.0.2]
        - go_log2csv_gamania_bgo.go 全部改成 streamer_3 [0.2.2]
        - 使用 functions來 parsing
        - 加上 json_config.json, source_csv.go, source_json.go, source_url.go
        - run_gamania_bgo.sh 多加 export LD_LIBRARY_PATH=$(pwd): 可以直接 load 進 .so
        - Makefile 改成可以compile .so 
