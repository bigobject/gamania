﻿package main

//go build -buildmode=plugin functions.go

import (
	"fmt"
	"log"
	"strconv"
	"strings"
	"time"
)

//var (
//	funcs              = map[string]func(record map[string]string, key string, input string, output_parse map[string]interface{}) (output interface{}, row_success bool, err error){"Datetime2Timestamp": Datetime2Timestamp, "DatetimeFormat": DatetimeFormat, "Regex": Regex, "Value": Value}
//	CacheRegex         map[string]interface{}
//	Parse_result_cache = make(map[string]interface{})

//	func_key = map[string]func(map[string]string) (bool, error) {
//		"stm":			get_stm_umillisec
//		, "umillisec":	get_stm_umillisec
//		, "uid":		get_uid_openid
//		, "action":		Get_action
//		, "verify_key":	Get_verify_key
//		}
//)

//=================================gamania below=============================
/*
func Parse_test(record map[string]string, key string, input string, output_parse map[string]interface{}) (output interface{}, row_success bool, err error) {
	if output, exist := Parse_result_cache[key]; !exist {
		if row_success, err = func_key(record); row_success {
			output = Parse_result_cache[key]
		}
	}
	return output, row_success, err
}
*/
func Get_stm_umillisec(records map[string]map[string]string, target_table string, target_key string, bo_fields_map_ver map[string][]map[string]interface{}, boColOrder_ver map[string]map[int]int, boCol map[string]map[string]int, Parse_result_cache map[string]map[string]string) (interface{}, map[string]map[string]string, bool, error) {

	/*
		$$
			input from records:
				stm
				time_sys
				dbtime
				dtm
			input from parse_result:

			output:
				stm
				umillisec
	*/
	var output interface{}
	row_success := true
	var err error
	var exist bool

	if output, exist = Parse_result_cache[target_table][target_key]; !exist {
		umillisec := ""
		time_sys := records[target_table]["time_sys"]
		//	dbtime := records[target_table]["dbtime"]
		var dbtime string
		if dbtime, exist = Parse_result_cache[target_table]["dbtime"]; !exist {
			var temp interface{}
			if temp, Parse_result_cache, row_success, err = Get_dbtime(records, target_table, "dbtime", bo_fields_map_ver, boColOrder_ver, boCol, Parse_result_cache); row_success {
				dbtime = temp.(string) //Parse_result_cache[target_table]["dbtime"]
			} else {
				return output, Parse_result_cache, row_success, err
			}
		}

		stm_with_t := records[target_table]["stm"] // input
		dtm := records[target_table]["dtm"]
		// $$ ==============================================================

		stm := ""
		// ?? Leo: 這種上午下午的格式還有嗎?
		if strings.Index(stm_with_t, "T下午") != -1 || strings.Index(stm_with_t, "T上午") != -1 {
			stm_with_t = strings.ReplaceAll(strings.ReplaceAll(stm_with_t, "T下午", "下午"), "T上午", "上午") //
			source := strings.ReplaceAll(strings.ReplaceAll(stm_with_t, "上午", "AM"), "下午", "PM")
			time_format := "2006-01-02PM15:04:05"
			stm = convertformat(time_format, source)
		} else {
			stm = strings.ReplaceAll(stm_with_t, "T", " ")
		}

		if strings.Index(stm, "/") != -1 {
			stm = strings.ReplaceAll(stm, "/", "-")
		}
		//================
		if stm == "" {
			umillisec = dtm
			stm = time_sys
		} else if strings.Index(stm, ":") == -1 || strings.Index(stm, "-") == -1 {
			umillisec = stm
			stm, err = convert_unixtime_to_datetime(stm, "mili-sec")
		} else {
			decimals := strings.Index(stm, ".")
			stm_decimal := ""
			if decimals != -1 {
				stm_decimal = stm[strings.Index(stm, ".") : strings.Index(stm, ".")+4]
			}
			stm = time_sys + stm_decimal

			t_parsing, temp_err := time.Parse("2006-01-02 15:04:05", stm)
			if temp_err != nil {
				row_success = false
				err = print_err_msg(temp_err, ": stm time parse fail: "+stm)
				return output, Parse_result_cache, row_success, err
			}
			unixNano := t_parsing.UnixNano()
			umillisec = strconv.FormatInt(int64(unixNano/1000000), 10)
		}
		if stm != "" && (stm > dbtime || stm > time_sys) { // 如果是未來的時間
			stm = time_sys
		} else if stm < "2019-06-30 00:00:00" {
			stm = time_sys
		} else if stm == "" {
			row_success = false
			err = print_err_msg(nil, "empty stm")
			return output, Parse_result_cache, row_success, err
		}
		// $$ ==============================================================
		Parse_result_cache[target_table]["stm"] = stm
		Parse_result_cache[target_table]["umillisec"] = umillisec

		output = Parse_result_cache[target_table][target_key]
	}

	return output, Parse_result_cache, row_success, err
}

func Get_uid(records map[string]map[string]string, target_table string, target_key string, bo_fields_map_ver map[string][]map[string]interface{}, boColOrder_ver map[string]map[int]int, boCol map[string]map[string]int, Parse_result_cache map[string]map[string]string) (interface{}, map[string]map[string]string, bool, error) {
	// $$ record: 整筆record, record裡的key和_bo裡的fieldname要一樣
	// $$ input: 單一目標欄位
	// $$ output_parse: _parse整個struct, 此function需要的parameter也從這裡拿. ex: output_parse["parameter_name"]

	/*
		$$
			input from records:
				uid
			input from parse_result:
				U.openid
			output:
				uid
	*/
	var output interface{}
	row_success := true
	var err error
	var exist bool

	if output, exist = Parse_result_cache[target_table][target_key]; !exist {

		uid := records[target_table]["uid"]

		// $$ ==============================================================
		var action_detail_openid string
		var temp interface{}
		if action_detail_openid, exist = Parse_result_cache[target_table]["U.openid"]; !exist {

			if temp, Parse_result_cache, row_success, err = Set_null_to_blank(records, target_table, "U.openid", bo_fields_map_ver, boColOrder_ver, boCol, Parse_result_cache); row_success {
				action_detail_openid = temp.(string)
			} else {
				return output, Parse_result_cache, row_success, err
			}
		}

		if uid == "" && action_detail_openid == "" {
			row_success = false
			err = print_err_msg(nil, "uid and Openid are blank")
			return output, Parse_result_cache, row_success, err
		}

		// $$ ==============================================================
		Parse_result_cache[target_table]["uid"] = uid
		Parse_result_cache[target_table]["U.openid"] = action_detail_openid

		output = Parse_result_cache[target_table][target_key]
	}

	return output, Parse_result_cache, row_success, err
}

func Get_action(records map[string]map[string]string, target_table string, target_key string, bo_fields_map_ver map[string][]map[string]interface{}, boColOrder_ver map[string]map[int]int, boCol map[string]map[string]int, Parse_result_cache map[string]map[string]string) (interface{}, map[string]map[string]string, bool, error) {

	/*
		$$
			input from records:
				action

			input from parse_result:

			output:
				action
	*/

	var output interface{}
	row_success := true
	var err error
	var exist bool

	if output, exist = Parse_result_cache[target_table][target_key]; !exist {
		// $$ ==============================================================
		action := records[target_table][target_key]
		if action == "category_action" {
			row_success = false
			err = print_err_msg(nil, "action is category_action")
			return output, Parse_result_cache, row_success, err
		}
		// $$ ==============================================================
		Parse_result_cache[target_table][target_key] = action
		output = Parse_result_cache[target_table][target_key]
	}

	return output, Parse_result_cache, row_success, err

}

/*
func Get_verify_key(records map[string]map[string]string, target_table string, target_key string, bo_fields_map_ver map[string][]map[string]interface{}, boColOrder_ver map[string]map[int]int, boCol map[string]map[string]int, Parse_result_cache map[string]map[string]string) (interface{}, map[string]map[string]string, bool, error) {


		$$
			input from records:
				verify_key

			input from parse_result:

			output:
				action

	var output interface{}
	row_success := true
	var err error
	var exist bool

	if output, exist = Parse_result_cache[target_table][target_key]; !exist {
		// $$ ==============================================================
		verify_key := records[target_table][target_key]
		if verify_key ==  "ebcc6fcb25c768a98ebf62f53541c97" {
			fmt.Println(co_userId)
		}
		// $$ ==============================================================
		Parse_result_cache[target_table][target_key] = verify_key
		output = Parse_result_cache[target_table][target_key]
	}

	return output, Parse_result_cache, row_success, err
}
*/

func Rounding_for_gamania(records map[string]map[string]string, target_table string, target_key string, bo_fields_map_ver map[string][]map[string]interface{}, boColOrder_ver map[string]map[int]int, boCol map[string]map[string]int, Parse_result_cache map[string]map[string]string) (interface{}, map[string]map[string]string, bool, error) {

	var output interface{}
	row_success := true
	var err error
	//	var exist bool

	//	if output, exist = Parse_result_cache[target_table][target_key]; !exist {
	// $$ ==============================================================
	var input_string string = records[target_table][target_key]
	output = input_string // $$ 不管怎樣先把input丟給output 以防失敗
	var temp interface{}
	if temp, err = Rounding_Decimals(input_string); err == nil {
		output = temp
	}
	// $$ ==============================================================
	Parse_result_cache[target_table][target_key] = InterfaceToStr(output) //.(string)
	output = Parse_result_cache[target_table][target_key]
	//	}
	return output, Parse_result_cache, row_success, err
}

func Get_msg_received_time(records map[string]map[string]string, target_table string, target_key string, bo_fields_map_ver map[string][]map[string]interface{}, boColOrder_ver map[string]map[int]int, boCol map[string]map[string]int, Parse_result_cache map[string]map[string]string) (interface{}, map[string]map[string]string, bool, error) {

	/*
		$$
			input from records:
				msg_received_time
			input from parse_result:

			output:
				msg_received_time
	*/
	var output interface{}
	row_success := true
	var err error
	var exist bool
	if output, exist = Parse_result_cache[target_table][target_key]; !exist {
		// $$ ==============================================================
		var input_string string = records[target_table][target_key]
		output, err = convert_unixtime_to_datetime(input_string, "mili-sec")
		// $$ ==============================================================
		Parse_result_cache[target_table][target_key] = output.(string)
		output = Parse_result_cache[target_table][target_key]
	}
	return output, Parse_result_cache, row_success, err
}

func Set_null_to_blank(records map[string]map[string]string, target_table string, target_key string, bo_fields_map_ver map[string][]map[string]interface{}, boColOrder_ver map[string]map[int]int, boCol map[string]map[string]int, Parse_result_cache map[string]map[string]string) (interface{}, map[string]map[string]string, bool, error) {

	var output interface{}
	row_success := true
	var err error
	//	var exist bool
	//	if output, exist = Parse_result_cache[target_table][target_key]; !exist {
	// $$ ==============================================================
	var input_string string = records[target_table][target_key]
	output = input_string
	if input_string == "null" || input_string == "NULL" {
		output = ""
	}
	// $$ ==============================================================
	Parse_result_cache[target_table][target_key] = output.(string)
	output = Parse_result_cache[target_table][target_key]
	//	}
	return output, Parse_result_cache, row_success, err
}

func To_Lower_Case(records map[string]map[string]string, target_table string, target_key string, bo_fields_map_ver map[string][]map[string]interface{}, boColOrder_ver map[string]map[int]int, boCol map[string]map[string]int, Parse_result_cache map[string]map[string]string) (interface{}, map[string]map[string]string, bool, error) {

	var output interface{}
	row_success := true
	var err error
	//	var exist bool
	//	if output, exist = Parse_result_cache[target_table][target_key]; !exist {
	// $$ ==============================================================
	var input_string string = records[target_table][target_key]
	output = strings.ToLower(input_string)
	// $$ ==============================================================
	Parse_result_cache[target_table][target_key] = output.(string)
	output = Parse_result_cache[target_table][target_key]
	//	}
	return output, Parse_result_cache, row_success, err
}

func Get_dbtime(records map[string]map[string]string, target_table string, target_key string, bo_fields_map_ver map[string][]map[string]interface{}, boColOrder_ver map[string]map[int]int, boCol map[string]map[string]int, Parse_result_cache map[string]map[string]string) (interface{}, map[string]map[string]string, bool, error) {

	/*
		$$
			input from records:

			input from parse_result:

			output:
				dbtime
	*/
	var output interface{}
	row_success := true
	var err error
	var exist bool
	if output, exist = Parse_result_cache[target_table][target_key]; !exist {
		// $$ ==============================================================
		//	var input_string string = records[target_table][target_key]
		timeNow := time.Now()
		output = fmt.Sprintf("%04d-%02d-%02d %02d:%02d:%02d", timeNow.Year(), timeNow.Month(), timeNow.Day(), timeNow.Hour(), timeNow.Minute(), timeNow.Second())
		// $$ ==============================================================
		Parse_result_cache[target_table][target_key] = output.(string)
		output = Parse_result_cache[target_table][target_key]
	}
	return output, Parse_result_cache, row_success, err
}

func Get_ts(records map[string]map[string]string, target_table string, target_key string, bo_fields_map_ver map[string][]map[string]interface{}, boColOrder_ver map[string]map[int]int, boCol map[string]map[string]int, Parse_result_cache map[string]map[string]string) (interface{}, map[string]map[string]string, bool, error) {

	/*
		$$
			input from records:
				time_sys
			input from parse_result:

			output:
				ts
	*/
	var output interface{}
	row_success := true
	var err error
	var exist bool
	if output, exist = Parse_result_cache[target_table][target_key]; !exist {
		// $$ ==============================================================
		//	var input_string string = records[target_table][target_key]
		output = "0"
		time_sys := records[target_table]["time_sys"]
		BO_TmFmt := "2006-01-02 15:04:05"
		if t, temp_err := time.Parse(BO_TmFmt, time_sys); temp_err == nil {
			h, _ := time.ParseDuration("-8h") //-8h Make UTC time
			t = t.Add(h)
			output = strconv.FormatInt(t.Unix(), 10)
		} else {
			err = temp_err
		}

		// $$ ==============================================================
		Parse_result_cache[target_table][target_key] = output.(string)
		output = Parse_result_cache[target_table][target_key]
	}
	return output, Parse_result_cache, row_success, err
}

func Assign_Value(records map[string]map[string]string, target_table string, target_key string, bo_fields_map_ver map[string][]map[string]interface{}, boColOrder_ver map[string]map[int]int, boCol map[string]map[string]int, Parse_result_cache map[string]map[string]string) (interface{}, map[string]map[string]string, bool, error) {

	var output interface{}
	row_success := true
	var err error
	//	var exist bool
	//	if output, exist = Parse_result_cache[target_table][target_key]; !exist {
	// $$ ==============================================================
	//	var input_string string = records[target_table][target_key]

	//	order_in_table_setting := boCol[target_table][target_key]
	//	order_in_bo_struct := boColOrder_ver[target_table][order_in_table_setting]

	if output, err = get_parameter("value", target_table, target_key, bo_fields_map_ver, boColOrder_ver, boCol); err == nil {
		// bo_fields_map_ver[target_table][order_in_bo_struct]["Parse"].(map[string]interface{})["value"].(string)
		Parse_result_cache[target_table][target_key] = output.(string)
		output = Parse_result_cache[target_table][target_key]
	}
	//	output = strings.ToLower(input_string)
	// $$ ==============================================================

	//	}
	return output, Parse_result_cache, row_success, err
	//================================
	//	if output_parse["input"] != nil {
	//		output = record[output_parse["input"].(string)]
	//	} else {
	//		output = output_parse["value"].(string)
	//	}
}

//====================================
func get_parameter(parameter string, target_table string, target_key string, bo_fields_map_ver map[string][]map[string]interface{}, boColOrder_ver map[string]map[int]int, boCol map[string]map[string]int) (string, error) {
	var output string
	var err error

	var order_in_table_setting int
	var order_in_bo_struct int
	var exist bool

	if order_in_table_setting, exist = boCol[target_table][target_key]; !exist {
		err = fmt.Errorf(target_table + " " + target_key + " does not exist in boCol")
	} else if order_in_bo_struct, exist = boColOrder_ver[target_table][order_in_table_setting]; !exist {
		err = fmt.Errorf(target_table + " " + strconv.Itoa(order_in_table_setting) + " does not exist in boColOrder_ver")
	} else if temp_parse := bo_fields_map_ver[target_table][order_in_bo_struct]["Parse"]; temp_parse == nil {
		err = fmt.Errorf(target_table + " " + target_key + " has no _parse")
	} else if temp_parameter := temp_parse.(map[string]interface{})[parameter]; temp_parameter == nil {
		err = fmt.Errorf(target_table + " " + target_key + " has no parameter " + parameter)
	} else {
		output = temp_parameter.(string)
	}

	return output, err
}
func Rounding_Decimals(input interface{}) (output interface{}, err error) {
	switch input.(type) {
	case int64:
		output = input.(int64)
	case int:
		output = input.(int)
	case float64:
		output = float64(int64(input.(float64)))
	case float32:
		output = float32(int64(input.(float64)))
	case string:
		temp := input.(string)
		if decimal_index := strings.Index(temp, "."); decimal_index != -1 {
			// $$ 有小數點
			temp = temp[:decimal_index]
		}
		output, err = strconv.ParseInt(temp, 10, 64)
	default:
		err = fmt.Errorf(fmt.Sprintf("InterFace type %T when rounding decimals", input))
	}
	return output, err
}

func print_err_msg(input_error error, input_msg string) error {
	// $$ 把 input_error 和 input_msg 結合成一個新的 error, 並且印出來
	// $$ output 應該會是 input_error: input_msg 或 input_err 或 input_msg 或 nil
	var err error
	var temp string
	if input_error != nil {
		temp = input_error.Error()
	}
	if input_msg != "" {
		if temp != "" {
			temp = temp + ": "
		}
		temp = temp + input_msg
	}
	if temp != "" {
		fmt.Println(temp)
		err = fmt.Errorf(temp)
	}
	return err
}

func convertformat(time_format, source string) string {
	var p time.Time
	var err error
	p, err = time.Parse(time_format, source)
	if err != nil {
		fmt.Println(err)
	}
	return p.Format("2006-01-02 15:04:05")
}

func convert_unixtime_to_time(input interface{}, format string) (output time.Time, err error) {

	// $$ input: 1562137224094
	// $$ output: 2019-07-03 07:00:24.094 +0000 UTC

	var unixtime_int64 int64
	var UnixTime_time time.Time
	switch input.(type) {
	case int64:
		unixtime_int64 = input.(int64)
	case int:
		unixtime_int64 = int64(input.(int))
	case string:
		unixtime_int64, err = strconv.ParseInt(input.(string), 10, 64)
	case float64:
		unixtime_int64 = int64(input.(float64))
	default:
		temp_string := "in convert_unixtime_to_time: InterFace type convert to int64 error: " + input.(string)
		err = fmt.Errorf(temp_string)
		unixtime_int64 = 0

	}
	if err != nil {
		var exponent int64 // float64
		switch format {
		case "sec":
			exponent = 1000000000 // 來源為 10 位
		case "mili-sec":
			exponent = 1000000 // 來源為 13 位
		case "nano-sec":
			exponent = 1 // 來源為 19 位
		default:
			err = fmt.Errorf("in convert_unixtime_to_time: invalid sec format") // append_error(err, fmt.Errorf("in convert_unixtime_to_time: invalid sec format"))
		}
		nano_time_stamp := unixtime_int64 * exponent // int64(math.Pow(10, exponent))

		UnixTime_time = time.Unix(0, nano_time_stamp) // $$ convert nano-second to time format
	}

	return UnixTime_time, err // $$ output looks like 2019-07-03 07:00:24.094 +0000 UTC
}

func convert_unixtime_to_datetime(input interface{}, format string) (string, error) {
	// $$ input: 13 位時間 1561715875951
	// $$ output: YYYY-MM-DD hh:mm:ss
	UnixTime_time, err := convert_unixtime_to_time(input, format)

	BO_TmFmt := "2006-01-02 15:04:05"
	var BOTz *time.Location
	if BOTz, err = time.LoadLocation("Asia/Taipei"); err != nil {
		log.Fatalln("Load TW time zone fail:", err)
	}
	output := UnixTime_time.In(BOTz).Format(BO_TmFmt)
	return output, err
}

func InterfaceToStr(value interface{}) string /*, error */ {

	switch value.(type) {
	case string:
		return value.(string) //, nil
	case []string:
		return value.([]string)[0] //, nil
	case int:
		return strconv.Itoa(value.(int)) //, nil
	case int32:
		return strconv.Itoa(int(value.(int32))) //, nil
	case int64:
		return strconv.FormatInt(int64(value.(int64)), 10) //, nil
	case float64:
		return strconv.FormatFloat(value.(float64), 'f', -1, 64) //, nil
	case nil:
		return ""
	default:
		temp := fmt.Sprintf("InterFace type %T convert to string error,\n", value)
		log.Println(temp)
		return "" //, fmt.Errorf(temp)
	}
}

//=================================gamania above=============================

//========================Jenny below===========================================

/*
stdLongMonth      = "January"
stdMonth          = "Jan"
stdNumMonth       = "1"
stdZeroMonth      = "01"
stdLongWeekDay    = "Monday"
stdWeekDay        = "Mon"
stdDay            = "2"
stdUnderDay       = "_2"
stdZeroDay        = "02"
stdHour           = "15"
stdHour12         = "3"
stdZeroHour12     = "03"
stdMinute         = "4"
stdZeroMinute     = "04"
stdSecond         = "5"
stdZeroSecond     = "05"
stdLongYear       = "2006"
stdYear           = "06"
stdPM             = "PM"
stdpm             = "pm"
stdTZ             = "MST"
stdISO8601TZ      = "Z0700"  // prints Z for UTC
stdISO8601ColonTZ = "Z07:00" // prints Z for UTC
stdNumTZ          = "-0700"  // always numeric
stdNumShortTZ     = "-07"    // always numeric
stdNumColonTZ     = "-07:00" // always numeric
*/

/*
func Datetime2Timestamp(record map[string]string, key string, input string, output_parse map[string]interface{}) (output interface{}, row_success bool, err error) {

	//get input datetime format
	row_success = true
	tz, err := strconv.Atoi(output_parse["timezone"].(string))
	if err != nil {
		fmt.Println(err)
		return nil, row_success, err
	}

	//calculate timezone difference
	hr := fmt.Sprintf("%dh", tz*-1)
	tzdiff, err := time.ParseDuration(hr)
	if err != nil {
		fmt.Println(err)
		return nil, row_success, err
	}

	//calculate local datetime
	datetime, err := time.Parse(output_parse["dateformat"].(string), input)
	if err != nil {
		fmt.Println(err)
		return nil, row_success, err
	}
	datetime = datetime.Add(tzdiff)

	//return timestamp
	return strconv.FormatInt(int64(datetime.Unix()), 10), row_success, err
}

func DatetimeFormat(record map[string]string, key string, input string, output_parse map[string]interface{}) (output interface{}, row_success bool, err error) {

	row_success = true
	//get input datetime format
	datetime, err := time.Parse(output_parse["dateformat"].(string), input)
	if err != nil {
		fmt.Println(err)
	}

	//return output datetime format
	return datetime.Format(output_parse["format"].(string)), row_success, err
}

func Regex(record map[string]string, key string, input string, output_parse map[string]interface{}) (output interface{}, row_success bool, err error) {

	row_success = true
	//get output regex position
	pos, _ := strconv.Atoi(output_parse["pos"].(string))

	//if regex cache exists
	if len(CacheRegex) > 0 && len(CacheRegex) < 10000 {
		if _, ok := CacheRegex[input]; ok {
			if CacheRegex[input].(map[string]interface{})["match"] == output_parse["match"].(string) {
				return CacheRegex[input].(map[string]interface{})["result"].(map[int]interface{})[pos].(interface{}), row_success, err
			}
		}
	} else { //clear cache memory when stored too many records
		CacheRegex = nil
	}

	//get regex formula
	re := regexp.MustCompile(output_parse["match"].(string))

	//find all match words
	match := re.FindStringSubmatch(input)
	if len(match) < pos+1 {
		return "", row_success, err
	}
	//fmt.Println("match:",match , "pos",pos, "output",match[pos])

	//create regex cache
	resulmap := make(map[int]interface{})
	for i, _ := range match {
		resulmap[i] = match[i]
		mp := map[string]interface{}{
			"match":  output_parse["match"].(string),
			"result": resulmap,
		}
		CacheRegex = map[string]interface{}{
			input: mp,
		}
	}

	return match[pos], row_success, err

}

func CheckCases(record map[string]string, key string, input string, output_parse map[string]interface{}) (output interface{}, row_success bool, err error) {

	row_success = true
	//get output all cases
	cases := output_parse["cases"].([]interface{})

	for _, v := range cases {

		casecheck := true
		casemap := v.(map[string]interface{})

		if casemap["condition_col"] != nil {
			colary := make([]string, len(casemap["condition_col"].([]interface{})))
			valary := make([]string, len(casemap["condition_val"].([]interface{})))

			for i, v := range v.(map[string]interface{})["condition_col"].([]interface{}) {
				colary[i] = fmt.Sprint(v)
			}

			for i, v := range v.(map[string]interface{})["condition_val"].([]interface{}) {
				valary[i] = fmt.Sprint(v)
				// fmt.Println("arrrr:",valary[i],record[colary[i]])
				if record[colary[i]] != valary[i] {
					casecheck = false
				}
			}
		}

		if casecheck || casemap["condition_col"] == nil {
			FuncName := casemap["func"].(string)

			a := funcs[FuncName]
			output, row_success, err = a(record, input, casemap)
			//fmt.Println(output)
			if output != "" {
				break
			}

		}

	}

	return output, row_success, err

}

//========================Jenny above===========================================

///======================== test below ===========================
func Test_leo_func(record map[string]string, key string, input string, output_parse map[string]interface{}) (output interface{}, row_success bool, err error) {
	// $$ record
	//get input datetime format
	row_success = true
	divide, err1 := strconv.Atoi(output_parse["divide"].(string))
	temp_input, err2 := strconv.Atoi(input)

	if err1 == nil && err2 == nil {
		output = temp_input / divide
	}
	fmt.Println("in test_leo_func ", temp_input, " / ", divide, " = ", output)
	//return output datetime format
	return output, row_success, err1
}

func Test_error(record map[string]string, key string, input string, output_parse map[string]interface{}) (output interface{}, row_success bool, err error) {

	row_success = true
	add_string, _ := output_parse["add_string"].(string)
	output = add_string
	fmt.Println("in test_error: ", output.(string))
	err = fmt.Errorf("sssaaa")
	return output, row_success, err
}

func Test_row_success(record map[string]string, key string, input string, output_parse map[string]interface{}) (output interface{}, row_success bool, err error) {

	success, _ := output_parse["sucess"].(bool)
	fmt.Println("success: ", success)
	row_success = success
	output = input
	fmt.Println("in test_error: ", output.(string))
	return output, row_success, err
}
*/
///======================== test above ===========================
