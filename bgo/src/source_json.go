package main

import (

	//"fmt"

	"reflect"
)

func get_JsonFieldName(data reflect.Value, fields []string) reflect.Value {

	/*
		$$ data reflect.Value 是一整個version的structure
		$$ fields []string 則是要找的那個欄位 藏在structure的哪裡的路徑圖: [Query Stm]

		用一個 for loop 從struct最上層開始找起, 等迴圈跑完, 就能找到我們要的值
	*/
	for _, field := range fields {
		data = data.FieldByName(field)
	}

	return data
}
