package main

import (
	"errors"
	"fmt"
	"reflect"
	"strconv"
)

// $$ reference https://play.golang.org/p/kwc32A5mJf
// ?? 把version_struct一整個structure傳進來, 用reader讀一行然後寫進version_struct裡面
func CsvUnmarshal(record []string, v reflect.Value) (success bool, marshal_err error) {
	//	record, err := reader.Read()
	//	if err != nil {
	//		fmt.Printf("csv unmarshall read fail: %v\n", err)
	//	}

	// $$ Leo: json讀進來的一行資料有標明每個欄位的fieldname是什麼
	// $$ , 但csv卻沒有, 是完全只能看順序, 所以 csv_config.json裡data_structure的順序不能亂掉, 必須要跟table_settings裡的順序一樣
	success = true
	var temp_err_string string
	fmt.Println("CsvUnmarshal: ")
	fmt.Println(record)
	if v.NumField() != len(record) {
		temp_err_string = "data structure number does not match csv table column number " + strconv.Itoa(v.NumField()) + " != " + strconv.Itoa(len(record))
		marshal_err = errors.New(temp_err_string)
		success = false
	}
	for i := 0; i < v.NumField(); i++ {
		fmt.Println("record[", i, "]: ", record[i])
		f := v.Field(i)
		switch f.Type().String() {
		case "string":
			f.SetString(record[i])
		case "int":
			ival, err := strconv.ParseInt(record[i], 10, 0)
			if err != nil {
				temp_err_string = "csv unmarshall parse int fail: " + err.Error()
				marshal_err = errors.New(temp_err_string)
				//	success = false
			}
			f.SetInt(ival)
		default:
			f.Set(reflect.ValueOf(record[i]))
		}
	}
	return success, marshal_err
}
